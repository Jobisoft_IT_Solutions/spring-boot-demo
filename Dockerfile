FROM openjdk:11-jre-slim

# note: heroku will define the port it requires the app to launch at via this env variable
ENV PORT=8080
ENV JAVA_OPTIONS="-Xms256m -Xmx256m"
ENV POSTGRES_USERNAME=NA
ENV POSTGRES_PASSWORD=NA

EXPOSE $PORT
VOLUME /tmp
COPY build/libs/app.jar app.jar

# note: heroku requires CMD instead of ENTRYPOINT
CMD java -Djava.security.egd=file:/dev/./urandom \
                -XshowSettings:vm \
                $JAVA_OPTIONS \
                -Dserver.port="$PORT" \
                -Dspring.datasource.username="$POSTGRES_USERNAME" \
                -Dspring.datasource.password="$POSTGRES_PASSWORD" \
                -jar /app.jar
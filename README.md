# Jobisoft IT Solutions - Spring Boot Demo

[![pipeline status](https://gitlab.com/Jobisoft_IT_Solutions/spring-boot-demo/badges/master/pipeline.svg)](https://gitlab.com/Jobisoft_IT_Solutions/spring-boot-demo/commits/master)  [![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=spring-boot-demo&metric=alert_status)](https://sonarcloud.io/dashboard?id=spring-boot-demo) [![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=spring-boot-demo&metric=sqale_rating)](https://sonarcloud.io/dashboard?id=spring-boot-demo) [![Reliability Rating](https://sonarcloud.io/api/project_badges/measure?project=spring-boot-demo&metric=reliability_rating)](https://sonarcloud.io/dashboard?id=spring-boot-demo) [![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=spring-boot-demo&metric=security_rating)](https://sonarcloud.io/dashboard?id=spring-boot-demo) [![Coverage](https://sonarcloud.io/api/project_badges/measure?project=spring-boot-demo&metric=coverage)](https://sonarcloud.io/dashboard?id=spring-boot-demo)

This project should demonstrate the code quality you can expect if you engage Jobisoft IT Solutions.
 
It has been implemented by me for a job application within 2 days and includes also a [continuous deployment pipeline](https://gitlab.com/Jobisoft_IT_Solutions/spring-boot-demo/blob/master/.gitlab-ci.yml) for GitLab 
which creates a Docker image that is [deployed automatically](https://gitlab.com/Jobisoft_IT_Solutions/spring-boot-demo/pipelines/88157579) to [Heroku](https://jobisoft-spring-boot-demo.herokuapp.com/).

Please note that I use a free tier of Heroku which means that the VM is shut down if not used and restarted on demand which takes 
around 30 seconds for the initial request.

## Assignment

The task was to create a small statistics microservice which is called from another support ticket microservice that is already in place.
The statistics microservice should have [REST endpoints](https://gitlab.com/Jobisoft_IT_Solutions/spring-boot-demo/tree/master/src/main/java/com/jobisoft/demo/springboot/controller/v1) for creating and transitioning support tickets through 
their states (open, waiting, solved). The main focus should be on an endpoint to get the number of tickets in each of 
the ticket statuses on a specific date. This endpoint should be performant even with high ticket numbers since we expect to have 1000 new 
open tickets per day. Also, the project should be deployed automatically via GitLab to some cloud provider.

## Solution

I created a very simple Spring Boot 2 WebMVC project using [PostgreSQL](https://gitlab.com/Jobisoft_IT_Solutions/spring-boot-demo/tree/master/src/main/resources/db/migration) and ensured that the performance constraints are met by integrated [performance tests](https://gitlab.com/Jobisoft_IT_Solutions/spring-boot-demo/tree/master/src/test/java/com/jobisoft/demo/springboot/performance).
I first tested the naive approach of calculating all the ticket transitions on the fly in PostgreSQL. This was not performant enough when using millions of 
entries, even with indexes. So I introduced an additional table that contains the delta for each ticket status for every single day. This allows us to get 
the required numbers very efficiently by just summing them up from the beginning. Also it allows future use cases like queries for a specific year, month or 
any other date range.

## Developer Setup

Clone the project from gitlab via:
 
`clone git@gitlab.com:Jobisoft_IT_Solutions/spring-boot-demo.git`

### Prerequisites

You need the following tools installed locally:
* JDK 11
* Gradle 5 (or you use the gradle wrapper contained in this project)
* For your IDE you might need a special plugin to support Lombok (for getter and setter generation) properly

### Installing

1. Build the project with `gradle clean build` to see if everything works correctly

2. Launch the application by right clicking on the StatisticsServiceApplication.java -> Run or by executing `gradle bootRun`

3. Navigate to <http://localhost:8080> 


## Running the tests

The tests can be executed from within your IDE or by executing `gradle clean check`.

You can find the generated test report in `[projectDir]/build/reports/tests/test/index.html`.

To generate a test coverage report you can execute `gradle clean build jacocoTestReport`.

The generated test coverage report can be found in `[projectDir]/build/reports/jacoco/test/html/index.html`

## Deployment

The application can be deployed via Docker or as a fat JAR.

### Docker

Just run `docker build . -t spring-boot-demo:1.0.0 .` in your project root directory to create the docker image.
Then you can run the image with `docker run spring-boot-demo:1.0.0`.

### Fat JAR

Just run `gradle clean build` to create the fat JAR. You can find it in `[projectDir]/build/libs`.

It can be executed via `./app.jar` or `java -jar app.jar`.


## REST Endpoints

All endpoints can be tested interactively on the swagger UI after launching the application. Just navigate to: <http://localhost:8080>

### Retrieve Statistics

```
GET /v1/statistics?date=2019-03-10 
```

### Add a new Ticket

```
POST /v1/tickets/SUP-1

{
  "creationTimestamp": "2019-03-09T18:20:30.145Z",
  "status": "OPEN"
} 
```

### Transition a Ticket

```
POST /v1/tickets/SUP-1/transition

{
  "newStatus": "OPEN",
  "oldStatus": "SOLVED",
  "transitionTimestamp": "2019-03-09T18:21:30.145Z"
}
```

## Authors

* **Johann Binder** - *Initial version* 
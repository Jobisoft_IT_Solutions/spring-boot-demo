package com.jobisoft.demo.springboot.controller.error;

import lombok.Data;

import java.io.Serializable;

/**
 * Response DTO which is used in error cases.
 *
 * TODO: can be made fancier by introducing separate status codes and error categories if needed
 *
 * @author Johann Binder
 */
@Data
public class ErrorMessage implements Serializable {
    private int errorCode;
    private String errorMessage;

    ErrorMessage(int errorCode, String errorMessage) {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }
}

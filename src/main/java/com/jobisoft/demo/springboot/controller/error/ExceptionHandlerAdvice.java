package com.jobisoft.demo.springboot.controller.error;

import com.jobisoft.demo.springboot.service.exception.InvalidTicketTransitionException;
import com.jobisoft.demo.springboot.service.exception.TicketDoesAlreadyExistException;
import com.jobisoft.demo.springboot.service.exception.TicketNotFoundException;
import com.jobisoft.demo.springboot.service.exception.TicketTransitionDoesAlreadyExistException;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.core.convert.ConversionFailedException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.stream.Collectors;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

/**
 * Exception handler which intercepts all exceptions that reach the controller level and converts them into
 * user-friendly ErrorMessage responses.
 *
 * @author Johann Binder
 */
@ControllerAdvice
public class ExceptionHandlerAdvice {
    /**
     * General and service-specific illegal arguments.
     *
     * @param e
     * @return
     */
    @ExceptionHandler({
            IllegalArgumentException.class,
            TicketDoesAlreadyExistException.class,
            TicketNotFoundException.class,
            InvalidTicketTransitionException.class,
            TicketTransitionDoesAlreadyExistException.class})
    public ResponseEntity<ErrorMessage> handleIllegalArgument(Throwable e) {
        return toErrorResponse(e, BAD_REQUEST);
    }

    /**
     * Enum and DateTime parse errors within DTOs.
     *
     * @param e
     * @return
     */
    @ExceptionHandler(ConversionFailedException.class)
    public ResponseEntity<ErrorMessage> handleConversionFailed(ConversionFailedException e) {
        return toErrorResponse("Parse error for value '" + e.getValue() + "'.", BAD_REQUEST);
    }

    /**
     * Enum and DateTime parse errors within DTOs.
     *
     * @param e
     * @return
     */
    @ExceptionHandler(InvalidFormatException.class)
    public ResponseEntity<ErrorMessage> handleInvalidFormat(InvalidFormatException e) {
        return toErrorResponse("Parse error for value '" + e.getValue() + "'.", BAD_REQUEST);
    }

    /**
     * @Valid validation errors on JSON properties (e.g. size restrictions).
     *
     * @param e
     * @return
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ErrorMessage> handleValidationError(MethodArgumentNotValidException e) {
        String message = e.getBindingResult().getAllErrors().stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
                .collect(Collectors.joining(" "));

        return toErrorResponse(message, BAD_REQUEST);
    }

    /**
     * Missing mandatory parameters.
     *
     * @param e
     * @return
     */
    @ExceptionHandler(MissingServletRequestParameterException.class)
    public ResponseEntity<ErrorMessage> handleMissingParameter(MissingServletRequestParameterException e) {
        String name = e.getParameterName();
        return toErrorResponse("Parameter '" + name + "' is missing.", BAD_REQUEST);
    }

    /**
     * Fallback method that catches the rest.
     * @param e
     * @return
     */
    @ExceptionHandler(Throwable.class)
    public ResponseEntity<ErrorMessage> handleGeneralError(Throwable e) {
        //if we can handle a more specific (hidden) exception, we will do so
        Throwable cause = e;
        while(cause.getCause() != null && cause.getCause() != cause) {
            cause = cause.getCause();
            if(cause instanceof IllegalArgumentException) {
                return handleIllegalArgument(cause);
            } else if(cause instanceof InvalidFormatException) {
                return handleInvalidFormat((InvalidFormatException) cause);
            } else if(cause instanceof ConversionFailedException) {
                return handleConversionFailed((ConversionFailedException) cause);
            }
        }

        return toErrorResponse(e, INTERNAL_SERVER_ERROR);
    }

    private ResponseEntity<ErrorMessage> toErrorResponse(Throwable e, HttpStatus httpStatusCode) {
        return toErrorResponse(e.getMessage(), httpStatusCode);
    }

    private ResponseEntity<ErrorMessage> toErrorResponse(String message, HttpStatus httpStatusCode) {
        ErrorMessage errorMessage = new ErrorMessage(httpStatusCode.value(), message);
        return new ResponseEntity<>(errorMessage, httpStatusCode);
    }
}
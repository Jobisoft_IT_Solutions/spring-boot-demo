package com.jobisoft.demo.springboot.controller.v1;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Redirects / to Swagger UI.
 *
 * @author Johann Binder
 */
@Controller
public class RootController {
    static final String SWAGGER_UI_URL = "/swagger-ui.html";

    @GetMapping("/")
    public String root() {
        return "redirect:" + SWAGGER_UI_URL;
    }
}

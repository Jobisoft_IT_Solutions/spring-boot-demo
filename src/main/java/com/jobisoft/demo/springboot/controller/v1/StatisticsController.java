package com.jobisoft.demo.springboot.controller.v1;

import com.jobisoft.demo.springboot.model.enums.TicketStatus;
import com.jobisoft.demo.springboot.service.StatisticsService;
import io.swagger.annotations.ApiParam;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.Map;

/**
 * Serves an endpoint to retrieve daily ticket statistics.
 *
 * @author Johann Binder
 */
@RestController
@RequestMapping(value = "/v1/statistics", produces = MediaType.APPLICATION_JSON_VALUE)
public class StatisticsController {
    private final StatisticsService statisticsService;

    public StatisticsController(StatisticsService statisticsService) {
        this.statisticsService = statisticsService;
    }

    @GetMapping
    public Map<TicketStatus, Long> getDailyTicketStatistics(
            @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) @ApiParam(defaultValue = "2019-10-09") LocalDate date) {
        return statisticsService.getDailyTicketStatistics(date);
    }
}
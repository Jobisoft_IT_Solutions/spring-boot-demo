package com.jobisoft.demo.springboot.controller.v1;

import com.jobisoft.demo.springboot.dto.TicketDTO;
import com.jobisoft.demo.springboot.dto.TicketTransitionDTO;
import com.jobisoft.demo.springboot.service.TicketService;
import io.swagger.annotations.ApiParam;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Serves endpoints to add new tickets and to transition tickets.
 *
 * @author Johann Binder
 */
@RestController
@RequestMapping(value = "/v1/tickets", produces = MediaType.APPLICATION_JSON_VALUE)
public class TicketController {
    private final TicketService ticketService;

    public TicketController(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    @PostMapping("/{ticketId}")
    @ResponseStatus(HttpStatus.CREATED)
    public void addTicket(@RequestBody @Valid TicketDTO ticket,
                          @PathVariable @ApiParam(defaultValue = "SUP-1") String ticketId) {
        ticket.setId(ticketId);
        ticketService.addTicket(ticket);
    }

    @PostMapping("/{ticketId}/transition")
    public void transitionTicket(@RequestBody @Valid TicketTransitionDTO transition,
                                 @PathVariable @ApiParam(defaultValue = "SUP-1") String ticketId) {
        if(transition.getOldStatus() == transition.getNewStatus()) {
            throw new IllegalArgumentException("The property 'oldStatus' has to be different from 'newStatus'.");
        }

        transition.setTicketId(ticketId);
        ticketService.transitionTicket(transition);
    }
}
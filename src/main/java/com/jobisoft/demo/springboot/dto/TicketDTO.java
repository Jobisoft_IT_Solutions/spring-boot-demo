package com.jobisoft.demo.springboot.dto;

import com.jobisoft.demo.springboot.model.enums.TicketStatus;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Request body DTO which is required for adding a new ticket.
 *
 * @author Johann Binder
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TicketDTO implements Serializable {
    @ApiModelProperty(hidden = true)
    private String id;

    @NotNull(message = "The property 'status' is missing.")
    @ApiModelProperty(example = "OPEN")
    private TicketStatus status;

    @NotNull(message = "The property 'creationTimestamp' is missing.")
    @PastOrPresent(message = "The value of 'creationTimestamp' must not be in the future.")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    @ApiModelProperty(example = "2019-10-09T18:20:30.145Z")
    private LocalDateTime creationTimestamp;
}

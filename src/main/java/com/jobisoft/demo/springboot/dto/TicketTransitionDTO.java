package com.jobisoft.demo.springboot.dto;

import com.jobisoft.demo.springboot.model.enums.TicketStatus;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Request body DTO which is required for transitioning a ticket.
 *
 * @author Johann Binder
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TicketTransitionDTO implements Serializable {
    @ApiModelProperty(hidden = true)
    private String ticketId;

    @NotNull(message = "The property 'oldStatus' is missing.")
    @ApiModelProperty(example = "OPEN")
    private TicketStatus oldStatus;

    @NotNull(message = "The property 'newStatus' is missing.")
    @ApiModelProperty(example = "SOLVED")
    private TicketStatus newStatus;

    @NotNull(message = "The property 'transitionTimestamp' is missing.")
    @PastOrPresent(message = "The value of 'transitionTimestamp' must not be in the future.")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    @ApiModelProperty(example = "2019-10-09T18:20:30.145Z")
    private LocalDateTime transitionTimestamp;
}

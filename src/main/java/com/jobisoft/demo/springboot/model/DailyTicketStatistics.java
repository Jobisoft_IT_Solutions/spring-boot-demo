package com.jobisoft.demo.springboot.model;

import com.jobisoft.demo.springboot.model.enums.TicketStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * This table is specific to the MVP use case. It stores aggregated diff values per ticket status for exactly one day.
 * Hence, it allows us to calculate statistics in a very performant way.
 *
 * Note: those diff values could also be negative, e.g. if 3 tickets changed from OPEN to something else
 * but only 1 new ticket has been opened on a day, the diff for the OPEN status on that day would be -2!
 *
 * @author Johann Binder
 */
@Entity
@Table(uniqueConstraints =
    @UniqueConstraint(columnNames = {"statisticsDate", "ticketStatus"})
)
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
public class DailyTicketStatistics {
    @Id
    @GeneratedValue(strategy=IDENTITY)
    @EqualsAndHashCode.Include
    private Long id;

    @NotNull
    private LocalDate statisticsDate;

    @NotNull
    @Enumerated
    private TicketStatus ticketStatus;

    @NotNull
    private Integer diff;

    public DailyTicketStatistics(LocalDate statisticsDate, TicketStatus ticketStatus, Integer diff) {
        this.statisticsDate = statisticsDate;
        this.ticketStatus = ticketStatus;
        this.diff = diff;
    }
}

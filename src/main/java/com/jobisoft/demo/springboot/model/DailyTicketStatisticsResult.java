package com.jobisoft.demo.springboot.model;

import com.jobisoft.demo.springboot.model.enums.TicketStatus;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * An aggregated row that represents how many tickets are in the given status.
 * Is used as result type of a JPQL query (more precisely: a List of these).
 *
 * @author Johann Binder
 */
@Data
@AllArgsConstructor
public class DailyTicketStatisticsResult {
    private TicketStatus ticketStatus;
    private Long quantity;
}

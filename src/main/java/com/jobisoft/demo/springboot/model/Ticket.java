package com.jobisoft.demo.springboot.model;

import com.jobisoft.demo.springboot.dto.TicketDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * A table representing which tickets are available in the system.
 * Contains only the ticket id itself at the moment but could be extended in the future.
 *
 * @author Johann Binder
 */
@Entity
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
public class Ticket {
    @Id
    @Size(max = 16)
    @EqualsAndHashCode.Include
    private String id;

    @OneToMany(mappedBy = "ticket", cascade = CascadeType.REMOVE)
    private List<TicketTransition> ticketTransition;

    public Ticket(@Size(max = 16) String id) {
        this.id = id;
    }

    public static Ticket from(TicketDTO ticket) {
        Ticket result = new Ticket();
        result.setId(ticket.getId());
        return result;
    }
}

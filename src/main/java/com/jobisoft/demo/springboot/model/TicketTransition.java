package com.jobisoft.demo.springboot.model;

import com.jobisoft.demo.springboot.dto.TicketTransitionDTO;
import com.jobisoft.demo.springboot.model.enums.CalculationType;
import com.jobisoft.demo.springboot.model.enums.TicketStatus;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

import static com.jobisoft.demo.springboot.model.enums.CalculationType.COMPENSATE;
import static com.jobisoft.demo.springboot.model.enums.CalculationType.NEW;
import static javax.persistence.GenerationType.IDENTITY;

/**
 * This table is just for backup purposes. It is the source set with that we can recalculate any other
 * table or view, if we need to. It won't be accessed with direct selects since it is too big and not performant enough.
 *
 * @author Johann Binder
 */
@Entity
@Table(uniqueConstraints =
    @UniqueConstraint(columnNames = {"ticket_id", "ticketStatus", "calculationValue", "transitionTimestamp"})
)
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
public class TicketTransition {
    @Id
    @GeneratedValue(strategy=IDENTITY)
    @EqualsAndHashCode.Include
    private Long id;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "ticket_id")
    private Ticket ticket;

    @NotNull
    @Enumerated
    private TicketStatus ticketStatus;

    /**
     * This field is hidden from direct external access.
     */
    @Min(-1)
    @Max(1)
    @Getter(AccessLevel.PRIVATE)
    @Setter(AccessLevel.PRIVATE)
    private Integer calculationValue;

    /**
     * This field is used to access calculationValue in a more user-friendly way.
     */
    @Transient
    private CalculationType calculationType;

    @NotNull
    private LocalDateTime transitionTimestamp;

    @NotNull
    private LocalDate transitionDate;

    @NotNull
    private LocalTime transitionTime;

    /**
     * Allows to set all fields of a transition.
     *
     * @param ticket
     * @param ticketStatus
     * @param calculationType
     * @param transitionTimestamp
     */
    public TicketTransition(Ticket ticket, TicketStatus ticketStatus, CalculationType calculationType, LocalDateTime transitionTimestamp) {     // NOSONAR
        this.ticket = ticket;
        this.ticketStatus = ticketStatus;
        this.calculationValue = calculationType.getCalculationValue();
        this.setTransitionTimestamp(transitionTimestamp);
    }

    public void setTransitionTimestamp(LocalDateTime transitionTimestamp) {
        this.transitionTimestamp = transitionTimestamp;
        if(transitionTimestamp != null) {
            this.transitionDate = transitionTimestamp.toLocalDate();
            this.transitionTime = transitionTimestamp.toLocalTime();
        }
    }

    public CalculationType getCalculationType() {  // NOSONAR
        return CalculationType.of(calculationValue);
    }

    public void setCalculationType(CalculationType calculationType) {  // NOSONAR
        this.calculationValue = calculationType.getCalculationValue();
    }

    /**
     * Creates both a transition entry that compensates the previous ticket state and
     * a new transition to the current state.
     *
     * @param ticketTransitionDto
     * @param ticket
     * @return
     */
    public static List<TicketTransition> from(TicketTransitionDTO ticketTransitionDto, Ticket ticket) {
        TicketTransition compensateOldTransition = new TicketTransition();
        compensateOldTransition.setTicket(ticket);
        compensateOldTransition.setTicketStatus(ticketTransitionDto.getOldStatus());
        compensateOldTransition.setCalculationType(COMPENSATE);
        compensateOldTransition.setTransitionTimestamp(ticketTransitionDto.getTransitionTimestamp());

        TicketTransition newTransition = new TicketTransition();
        newTransition.setTicket(ticket);
        newTransition.setTicketStatus(ticketTransitionDto.getNewStatus());
        newTransition.setCalculationType(NEW);
        newTransition.setTransitionTimestamp(ticketTransitionDto.getTransitionTimestamp());

        return List.of(compensateOldTransition, newTransition);

    }

    /**
     * Mainly for testing, assumes that all own fields are not null.
     * Doesn't care about the id.
     *
     * @param transition
     * @return
     */
    public boolean equalsAllFields(TicketTransition transition) {
        return transition != null
                && ticket.equals(transition.getTicket())
                && ticketStatus.equals(transition.getTicketStatus())
                && calculationValue.equals(transition.getCalculationValue())
                && transitionTimestamp.equals(transition.getTransitionTimestamp())
                && transitionDate.equals(transition.getTransitionDate())
                && transitionTime.equals(transition.getTransitionTime());
    }
}

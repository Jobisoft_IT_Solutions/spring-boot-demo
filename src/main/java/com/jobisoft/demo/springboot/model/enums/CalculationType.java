package com.jobisoft.demo.springboot.model.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

/**
 * Indicates whether a TicketTransition should be counted as a new transition (+1) or used to compensate an old one (-1).
 *
 * @author Johann Binder
 */
@Getter
@AllArgsConstructor
@ToString
public enum CalculationType {
    NEW(1),
    COMPENSATE(-1);

    private final int calculationValue;

    public static CalculationType of(int isCurrentStatus) {
        if(isCurrentStatus == 1) {
            return NEW;
        } else if (isCurrentStatus == -1) {
            return COMPENSATE;
        } else {
            throw new IllegalArgumentException("invalid calculationValue provided, only -1 or 1 are allowed: " + isCurrentStatus);
        }
    }
}

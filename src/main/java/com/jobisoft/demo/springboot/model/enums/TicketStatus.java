package com.jobisoft.demo.springboot.model.enums;

/**
 * All possible ticket status entries.
 *
 * @author Johann Binder
 */
public enum TicketStatus {
    OPEN,       //id 0
    SOLVED,     //id 1
    WAITING     //id 2
}

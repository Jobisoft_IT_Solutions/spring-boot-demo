package com.jobisoft.demo.springboot.repository;

import com.jobisoft.demo.springboot.model.DailyTicketStatistics;
import com.jobisoft.demo.springboot.model.DailyTicketStatisticsResult;
import com.jobisoft.demo.springboot.model.enums.TicketStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * Repository used for accessing DailyTicketStatistics entities.
 * Should be used for calculating daily ticket statistics in a performant way.
 *
 * @author Johann Binder
 */
public interface DailyTicketStatisticsRepository extends JpaRepository<DailyTicketStatistics, Long> {
    Optional<DailyTicketStatistics> findByStatisticsDateAndTicketStatus(LocalDate statisticsDate, TicketStatus ticketStatus);

    @Query( "SELECT new com.jobisoft.demo.springboot.model.DailyTicketStatisticsResult(s.ticketStatus, SUM(s.diff)) " +
            "FROM DailyTicketStatistics s " +
            "WHERE s.statisticsDate <= :date " +
            "GROUP BY s.ticketStatus " +
            "ORDER BY s.ticketStatus")
    List<DailyTicketStatisticsResult> calculateDailyTicketStatistics(@Param("date") LocalDate date);
}

package com.jobisoft.demo.springboot.repository;

import com.jobisoft.demo.springboot.model.Ticket;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repository used for accessing Ticket entities.
 *
 * @author Johann Binder
 */
public interface TicketRepository extends JpaRepository<Ticket, String> {
}

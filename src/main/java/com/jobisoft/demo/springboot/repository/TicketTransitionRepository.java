package com.jobisoft.demo.springboot.repository;

import com.jobisoft.demo.springboot.model.DailyTicketStatisticsResult;
import com.jobisoft.demo.springboot.model.Ticket;
import com.jobisoft.demo.springboot.model.TicketTransition;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * Repository used for accessing TicketTranstition entities.
 * Could also be used for calculating daily ticket statistics in a slow way (discouraged).
 *
 * @author Johann Binder
 */
public interface TicketTransitionRepository extends JpaRepository<TicketTransition, Long> {
    Optional<TicketTransition> findTopByTicketOrderByIdDesc(Ticket ticket);

    /**
     * This method is not performant enough. It takes around 16 seconds if we have 5 mio transitions in the table.
     *
     * @deprecated Use DailyTicketStatisticsRepository.calculateDailyTicketStatistics() instead.
     * @param date
     * @return
     */
    @Deprecated(since = "1.0.0")
    @Query( "SELECT new com.jobisoft.demo.springboot.model.DailyTicketStatisticsResult(t.ticketStatus, SUM(t.calculationValue)) " +
            "FROM TicketTransition t " +
            "WHERE t.transitionDate <= :date " +
            "GROUP BY t.ticketStatus " +
            "ORDER BY t.ticketStatus")
    List<DailyTicketStatisticsResult> calculateDailyTicketStatistics(@Param("date") LocalDate date);  // NOSONAR
}

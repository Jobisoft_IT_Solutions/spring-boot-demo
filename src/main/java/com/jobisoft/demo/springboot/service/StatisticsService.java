package com.jobisoft.demo.springboot.service;

import com.jobisoft.demo.springboot.model.enums.TicketStatus;

import java.time.LocalDate;
import java.util.Map;

/**
 * Service capable of returning daily ticket statistics.
 *
 * @author Johann Binder
 */
public interface StatisticsService {
    /**
     * Returns a map of "ticket status -> quantity" entries for all ticket statuses for the specified day.
     * @param date
     * @return
     */
    Map<TicketStatus, Long> getDailyTicketStatistics(LocalDate date);
}

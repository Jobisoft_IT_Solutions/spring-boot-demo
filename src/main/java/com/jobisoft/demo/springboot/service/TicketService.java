package com.jobisoft.demo.springboot.service;

import com.jobisoft.demo.springboot.dto.TicketDTO;
import com.jobisoft.demo.springboot.dto.TicketTransitionDTO;

/**
 * Service responsible for adding and transitioning tickets transactionally secure.
 *
 * @author Johann Binder
 */
public interface TicketService {
    /**
     * Adds the passed ticket to the ticketing system.
     * @param ticket
     */
    void addTicket(TicketDTO ticket);

    /**
     * Transitions the ticket in the system according to the passed transition info.
     * @param ticketTransition
     */
    void transitionTicket(TicketTransitionDTO ticketTransition);
}

package com.jobisoft.demo.springboot.service.exception;

/**
 * Will be thrown if requested ticket transition does not meet the business constraints.
 *
 * @author Johann Binder
 */
public class InvalidTicketTransitionException extends RuntimeException {
    public InvalidTicketTransitionException(String message) {
        super("Invalid ticket transition: " + message);
    }
}

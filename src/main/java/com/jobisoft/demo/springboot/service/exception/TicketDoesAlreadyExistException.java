package com.jobisoft.demo.springboot.service.exception;

/**
 * Will be thrown if the requested ticket cannot be added since it exists already in the system.
 *
 * @author Johann Binder
 */
public class TicketDoesAlreadyExistException extends RuntimeException {
    public TicketDoesAlreadyExistException(String id) {
        super("Ticket '" + id + "' does already exist.");
    }
}

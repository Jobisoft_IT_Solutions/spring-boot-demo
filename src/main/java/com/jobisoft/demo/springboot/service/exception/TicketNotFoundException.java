package com.jobisoft.demo.springboot.service.exception;

/**
 * Will be thrown if the required ticket cannot be found in the system.
 *
 * @author Johann Binder
 */
public class TicketNotFoundException extends RuntimeException {
    public TicketNotFoundException(String id) {
        super("Ticket '" + id + "' couldn't be found.");
    }
}

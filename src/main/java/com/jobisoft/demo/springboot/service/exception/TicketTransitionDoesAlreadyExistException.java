package com.jobisoft.demo.springboot.service.exception;

import com.jobisoft.demo.springboot.dto.TicketTransitionDTO;

/**
 * Will be thrown if the requested ticket transition is present already in the system.
 *
 * @author Johann Binder
 */
public class TicketTransitionDoesAlreadyExistException extends RuntimeException {
    public TicketTransitionDoesAlreadyExistException(TicketTransitionDTO transition) {
        super("Ticket transition ("
                + transition.getTicketId() + ", "
                + transition.getOldStatus() + ", "
                + transition.getNewStatus() + ", "
                + transition.getTransitionTimestamp()
                + ") does already exist.");
    }
}

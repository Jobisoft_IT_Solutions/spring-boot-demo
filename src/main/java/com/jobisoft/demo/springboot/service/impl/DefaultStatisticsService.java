package com.jobisoft.demo.springboot.service.impl;

import com.jobisoft.demo.springboot.model.DailyTicketStatisticsResult;
import com.jobisoft.demo.springboot.model.enums.TicketStatus;
import com.jobisoft.demo.springboot.repository.DailyTicketStatisticsRepository;
import com.jobisoft.demo.springboot.service.StatisticsService;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Default implementation of StatisticsService.
 *
 * @author Johann Binder
 */
@Service
public class DefaultStatisticsService implements StatisticsService {
    private final DailyTicketStatisticsRepository dailyTicketStatisticsRepository;

    public DefaultStatisticsService(DailyTicketStatisticsRepository dailyTicketStatisticsRepository) {
        this.dailyTicketStatisticsRepository = dailyTicketStatisticsRepository;
    }

    @Override
    public Map<TicketStatus, Long> getDailyTicketStatistics(@NonNull LocalDate date) {
        Assert.notNull(date, "date must not be null");

        List<DailyTicketStatisticsResult> results = dailyTicketStatisticsRepository.calculateDailyTicketStatistics(date);
        return results.stream().collect(Collectors.toMap(
                DailyTicketStatisticsResult::getTicketStatus,
                DailyTicketStatisticsResult::getQuantity));
    }
}

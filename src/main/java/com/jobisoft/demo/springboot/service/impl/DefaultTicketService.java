package com.jobisoft.demo.springboot.service.impl;

import com.jobisoft.demo.springboot.dto.TicketDTO;
import com.jobisoft.demo.springboot.dto.TicketTransitionDTO;
import com.jobisoft.demo.springboot.model.DailyTicketStatistics;
import com.jobisoft.demo.springboot.model.Ticket;
import com.jobisoft.demo.springboot.model.TicketTransition;
import com.jobisoft.demo.springboot.model.enums.TicketStatus;
import com.jobisoft.demo.springboot.repository.DailyTicketStatisticsRepository;
import com.jobisoft.demo.springboot.repository.TicketRepository;
import com.jobisoft.demo.springboot.repository.TicketTransitionRepository;
import com.jobisoft.demo.springboot.service.TicketService;
import com.jobisoft.demo.springboot.service.exception.InvalidTicketTransitionException;
import com.jobisoft.demo.springboot.service.exception.TicketDoesAlreadyExistException;
import com.jobisoft.demo.springboot.service.exception.TicketNotFoundException;
import com.jobisoft.demo.springboot.service.exception.TicketTransitionDoesAlreadyExistException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static com.jobisoft.demo.springboot.model.enums.CalculationType.NEW;

/**
 * Default implementation of TicketService.
 *
 * @author Johann Binder
 */
@Service
public class DefaultTicketService implements TicketService {
    private final TicketRepository ticketRepository;
    private final TicketTransitionRepository ticketTransitionRepository;
    private final DailyTicketStatisticsRepository dailyTicketStatisticsRepository;

    public DefaultTicketService(TicketRepository ticketRepository,
                                TicketTransitionRepository ticketTransitionRepository,
                                DailyTicketStatisticsRepository dailyTicketStatisticsRepository) {
        this.ticketRepository = ticketRepository;
        this.ticketTransitionRepository = ticketTransitionRepository;
        this.dailyTicketStatisticsRepository = dailyTicketStatisticsRepository;
    }

    @Override
    @Transactional
    public void addTicket(@NonNull TicketDTO ticketDto) {
        Assert.notNull(ticketDto, "ticketDto must not be null");

        Optional<Ticket> foundTicket = ticketRepository.findById(ticketDto.getId());
        if(foundTicket.isPresent()) {
            throw new TicketDoesAlreadyExistException(ticketDto.getId());
        }

        Ticket ticket = insertTicket(ticketDto);
        insertTicketTransition(ticketDto, ticket);
        insertOrUpdateStatistics(ticketDto);
    }

    private Ticket insertTicket(TicketDTO ticketDto) {
        Ticket ticket = Ticket.from(ticketDto);
        ticket = ticketRepository.save(ticket);
        return ticket;
    }

    private void insertTicketTransition(TicketDTO ticketDto, Ticket ticket) {
        TicketTransition transition = new TicketTransition(ticket,
                ticketDto.getStatus(), NEW, ticketDto.getCreationTimestamp());
        ticketTransitionRepository.save(transition);
    }

    private void insertOrUpdateStatistics(TicketDTO ticketDto) {
        Optional<DailyTicketStatistics> statistics = dailyTicketStatisticsRepository.findByStatisticsDateAndTicketStatus(
                ticketDto.getCreationTimestamp().toLocalDate(), ticketDto.getStatus());
        statistics.ifPresentOrElse(
                curStatistics -> updateStatistics(curStatistics, 1),
                () -> insertStatistics(
                        ticketDto.getCreationTimestamp().toLocalDate(),
                        ticketDto.getStatus(),
                        1));
    }

    private void updateStatistics(DailyTicketStatistics curStatistics, int diff) {
        curStatistics.setDiff(curStatistics.getDiff() + diff);
        dailyTicketStatisticsRepository.save(curStatistics);
    }

    private void insertStatistics(LocalDate date, TicketStatus status, int diff) {
        DailyTicketStatistics newStatistics = new DailyTicketStatistics(date, status, diff);
        dailyTicketStatisticsRepository.save(newStatistics);
    }

    @Override
    @Transactional
    public void transitionTicket(@NonNull TicketTransitionDTO ticketTransitionDto) {
        Assert.notNull(ticketTransitionDto, "ticketTransitionDto must not be null");

        Ticket ticket = validateTicketTransition(ticketTransitionDto);
        insertTicketTransitions(ticketTransitionDto, ticket);
        insertOrUpdateStatistics(ticketTransitionDto);
    }

    private Ticket validateTicketTransition(@NonNull TicketTransitionDTO ticketTransitionDto) {
        Optional<Ticket> foundTicket = ticketRepository.findById(ticketTransitionDto.getTicketId());
        Ticket ticket = foundTicket.orElseThrow(
                () -> new TicketNotFoundException(ticketTransitionDto.getTicketId()));

        Optional<TicketTransition> foundTicketTransition = ticketTransitionRepository
                .findTopByTicketOrderByIdDesc(ticket);
        TicketTransition lastTicketTransition = foundTicketTransition.orElseThrow(
                () -> new InvalidTicketTransitionException("Current ticket status could not be found in the system."));

        if(lastTicketTransition.getTransitionTimestamp().isAfter(ticketTransitionDto.getTransitionTimestamp())) {
            throw new InvalidTicketTransitionException("Current ticket transition is more current (" +
                    lastTicketTransition.getTransitionTimestamp() + ") than the provided one (" +
                    ticketTransitionDto.getTransitionTimestamp() + ").");
        }

        if(lastTicketTransition.getTicketStatus() != ticketTransitionDto.getOldStatus()) {
            throw new InvalidTicketTransitionException("Current ticket status (" + lastTicketTransition.getTicketStatus() +
                    ") doesn't match the provided one ('oldStatus': " + ticketTransitionDto.getOldStatus() + ").");
        }

        return ticket;
    }

    private void insertTicketTransitions(TicketTransitionDTO ticketTransitionDto, Ticket ticket) {
        try {
            List<TicketTransition> transitions = TicketTransition.from(ticketTransitionDto, ticket);
            ticketTransitionRepository.saveAll(transitions);
        } catch(DataIntegrityViolationException e) {
            throw new TicketTransitionDoesAlreadyExistException(ticketTransitionDto);
        }
    }

    private void insertOrUpdateStatistics(TicketTransitionDTO ticketTransitionDto) {
        LocalDate date = ticketTransitionDto.getTransitionTimestamp().toLocalDate();
        Optional<DailyTicketStatistics> oldStatistics = dailyTicketStatisticsRepository.findByStatisticsDateAndTicketStatus(
                date, ticketTransitionDto.getOldStatus());
        oldStatistics.ifPresentOrElse(
                curStatistics -> updateStatistics(curStatistics, -1),
                () -> insertStatistics(
                        date,
                        ticketTransitionDto.getOldStatus(),
                        -1
                ));

        Optional<DailyTicketStatistics> newStatistics = dailyTicketStatisticsRepository.findByStatisticsDateAndTicketStatus(
                date, ticketTransitionDto.getNewStatus());
        newStatistics.ifPresentOrElse(
                curStatistics -> updateStatistics(curStatistics, 1),
                () -> insertStatistics(
                        date,
                        ticketTransitionDto.getNewStatus(),
                        1
                ));
    }
}

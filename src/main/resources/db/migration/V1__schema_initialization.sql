create table ticket_status (
  id serial primary key,                    -- artificial PK so we can change ticketStatus names more easily
  name varchar(32) not null,
  unique (name)
);

create table ticket (                       -- might be useful for future requirements
  id varchar(16) primary key
);

create table ticket_transition (
  id bigserial primary key,
  ticket_id varchar(16) not null,
  ticket_status integer not null,
  calculation_value integer not null,       -- flag that is either -1 or +1, used for easier calculation
  transition_timestamp timestamp not null,
  transition_date date not null,            -- for performance increase due to index
  transition_time time not null,            -- might be useful for future requirements
  unique (ticket_id, ticket_status, calculation_value, transition_timestamp),
  foreign key (ticket_id) references ticket(id),
  foreign key (ticket_status) references ticket_status(id)
);

create index on ticket_transition(ticket_id, transition_timestamp);
create index on ticket_transition(transition_timestamp);
create index on ticket_transition(transition_time);
create index on ticket_transition(transition_date);
create index on ticket_transition(ticket_status);
create index on ticket_transition(ticket_status, calculation_value);

create table daily_ticket_statistics (
  id bigserial primary key,
  statistics_date date not null,
  ticket_status integer not null,
  diff integer not null,
  unique (statistics_date, ticket_status),
  foreign key (ticket_status) references ticket_status(id)
);

create index on daily_ticket_statistics(statistics_date);
create index on daily_ticket_statistics(ticket_status);
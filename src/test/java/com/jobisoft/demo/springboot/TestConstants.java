package com.jobisoft.demo.springboot;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * @author Johann Binder
 */
public class TestConstants {
    public static final String REPOSITORY_TESTS_PROFILE = "repository-tests";
    public static final String PERFORMANCE_TESTS_PROFILE = "performance-tests";

    public static final LocalDateTime NOW = LocalDateTime.now();
    public static final LocalDate TODAY = NOW.toLocalDate();
    public static final LocalDate YESTERDAY = TODAY.minusDays(1);
    public static final LocalDate TOMORROW = TODAY.plusDays(1);
}

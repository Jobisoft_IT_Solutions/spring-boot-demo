package com.jobisoft.demo.springboot.controller.v1;

import com.jobisoft.demo.springboot.model.enums.TicketStatus;
import com.jobisoft.demo.springboot.service.StatisticsService;
import com.jobisoft.demo.springboot.TestConstants;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;
import java.util.Map;

import static java.time.format.DateTimeFormatter.ISO_DATE;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.times;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * These tests use no real spring context so they can be executed faster.
 *
 * @author Johann Binder
 */
@RunWith(SpringRunner.class)
@WebMvcTest(StatisticsController.class)
public class StatisticsControllerTest {
    private static final String TODAY_ISO = TestConstants.TODAY.format(ISO_DATE);
    private static final Map<TicketStatus, Long> RESULT = Map.of(
            TicketStatus.OPEN, 100L,
            TicketStatus.SOLVED, 200L,
            TicketStatus.WAITING, 50L
    );

    @Autowired
    private MockMvc mvc;

    @MockBean
    private StatisticsService statisticsService;

    @Test
    public void testGetDailyTicketStatisticsSuccessful() throws Exception {
        //given
        when(statisticsService.getDailyTicketStatistics(TestConstants.TODAY))
                .thenReturn(RESULT);

        //when & then
        mvc.perform(get("/v1/statistics")
                .param("date", TODAY_ISO)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.OPEN", is(100)))
                .andExpect(jsonPath("$.SOLVED", is(200)))
                .andExpect(jsonPath("$.WAITING", is(50)));

        verify(statisticsService, times(1)).getDailyTicketStatistics(TestConstants.TODAY);
    }

    @Test
    public void testGetDailyTicketStatisticsEmptyResult() throws Exception {
        //given
        when(statisticsService.getDailyTicketStatistics(TestConstants.TODAY))
                .thenReturn(Collections.emptyMap());

        //when & then
        mvc.perform(get("/v1/statistics")
                .param("date", TODAY_ISO)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string("{}"));

        verify(statisticsService, times(1)).getDailyTicketStatistics(TestConstants.TODAY);
    }

    @Test
    public void testGetDailyTicketStatisticsWithoutDate() throws Exception {
        //given
        when(statisticsService.getDailyTicketStatistics(TestConstants.TODAY))
                .thenReturn(RESULT);

        //when & then
        mvc.perform(get("/v1/statistics")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errorCode", is(400)))
                .andExpect(jsonPath("$.errorMessage", is("Parameter 'date' is missing.")));

        verify(statisticsService, times(0)).getDailyTicketStatistics(any());
    }

    @Test
    public void testGetDailyTicketStatisticsWithInvalidDate() throws Exception {
        //given
        when(statisticsService.getDailyTicketStatistics(TestConstants.TODAY))
                .thenReturn(RESULT);

        //when & then
        mvc.perform(get("/v1/statistics")
                .param("date", "123")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errorCode", is(400)))
                .andExpect(jsonPath("$.errorMessage", is("Parse error for value '123'.")));

        verify(statisticsService, times(0)).getDailyTicketStatistics(any());
    }
}
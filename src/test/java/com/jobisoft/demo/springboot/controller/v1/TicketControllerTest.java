package com.jobisoft.demo.springboot.controller.v1;

import com.jobisoft.demo.springboot.dto.TicketDTO;
import com.jobisoft.demo.springboot.dto.TicketTransitionDTO;
import com.jobisoft.demo.springboot.service.TicketService;
import com.jobisoft.demo.springboot.service.exception.InvalidTicketTransitionException;
import com.jobisoft.demo.springboot.service.exception.TicketDoesAlreadyExistException;
import com.jobisoft.demo.springboot.TestConstants;
import com.jobisoft.demo.springboot.model.enums.TicketStatus;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static java.time.format.DateTimeFormatter.ISO_DATE_TIME;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.internal.verification.VerificationModeFactory.times;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * These tests use no real spring context so they can be executed faster.
 *
 * @author Johann Binder
 */
@RunWith(SpringRunner.class)
@WebMvcTest(TicketController.class)
public class TicketControllerTest {
    private static final String NOW_ISO = TestConstants.NOW.format(ISO_DATE_TIME);
    private static final TicketDTO TICKET_DTO = new TicketDTO("SUP-1", TicketStatus.OPEN, TestConstants.NOW);
    private static final TicketTransitionDTO TRANSITION_DTO = new TicketTransitionDTO("SUP-1", TicketStatus.OPEN, TicketStatus.SOLVED, TestConstants.NOW);

    @Autowired
    private MockMvc mvc;

    @MockBean
    private TicketService ticketService;

    @Test
    public void testAddTicketSuccessful() throws Exception {
        //given: no precondition

        //when & then
        mvc.perform(post("/v1/tickets/SUP-1")
                .content("{\n" +
                        "  \"creationTimestamp\": \"" + NOW_ISO + "\",\n" +
                        "  \"status\": \"" + TicketStatus.OPEN.name() + "\"\n" +
                        "}")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(content().string(""));

        verify(ticketService, times(1)).addTicket(TICKET_DTO);
    }

    @Test
    public void testAddTicketAlreadyExists() throws Exception {
        //given
        Mockito.doThrow(new TicketDoesAlreadyExistException("SUP-1"))
                .when(ticketService).addTicket(TICKET_DTO);

        //when & then
        mvc.perform(post("/v1/tickets/SUP-1")
                .content("{\n" +
                        "  \"creationTimestamp\": \"" + NOW_ISO + "\",\n" +
                        "  \"status\": \"" + TicketStatus.OPEN.name() + "\"\n" +
                        "}")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errorCode", is(400)))
                .andExpect(jsonPath("$.errorMessage", is("Ticket 'SUP-1' does already exist.")));

        verify(ticketService, times(1)).addTicket(TICKET_DTO);
    }

    @Test
    public void testAddTicketDateInFuture() throws Exception {
        //given: no preconditions

        //when & then
        mvc.perform(post("/v1/tickets/SUP-1")
                .content("{\n" +
                        "  \"creationTimestamp\": \"" + TestConstants.TOMORROW.atStartOfDay().format(ISO_DATE_TIME) + "\",\n" +
                        "  \"status\": \"" + TicketStatus.OPEN.name() + "\"\n" +
                        "}")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errorCode", is(400)))
                .andExpect(jsonPath("$.errorMessage", is("The value of 'creationTimestamp' must not be in the future.")));

        verify(ticketService, times(0)).addTicket(TICKET_DTO);
    }

    @Test
    public void testAddTicketInvalidDate() throws Exception {
        //given: no preconditions

        //when & then
        mvc.perform(post("/v1/tickets/SUP-1")
                .content("{\n" +
                        "  \"creationTimestamp\": \"asdf\",\n" +
                        "  \"status\": \"" + TicketStatus.OPEN.name() + "\"\n" +
                        "}")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errorCode", is(400)))
                .andExpect(jsonPath("$.errorMessage", is("Parse error for value 'asdf'.")));

        verify(ticketService, times(0)).addTicket(TICKET_DTO);
    }

    @Test
    public void testAddTicketInvalidTicketStatus() throws Exception {
        //given: no preconditions

        //when & then
        mvc.perform(post("/v1/tickets/SUP-1")
                .content("{\n" +
                        "  \"creationTimestamp\": \"" + NOW_ISO + "\",\n" +
                        "  \"status\": \"asdf\"\n" +
                        "}")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errorCode", is(400)))
                .andExpect(jsonPath("$.errorMessage", is("Parse error for value 'asdf'.")));

        verify(ticketService, times(0)).addTicket(TICKET_DTO);
    }

    @Test
    public void testTransitionTicketSuccessful() throws Exception {
        //given: no precondition

        //when & then
        mvc.perform(post("/v1/tickets/SUP-1/transition")
                .content("{\n" +
                        "  \"oldStatus\": \"" + TicketStatus.OPEN.name() + "\",\n" +
                        "  \"newStatus\": \"" + TicketStatus.SOLVED.name() + "\",\n" +
                        "  \"transitionTimestamp\": \"" + NOW_ISO + "\"\n" +
                        "}")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(""));

        verify(ticketService, times(1)).transitionTicket(TRANSITION_DTO);
    }

    @Test
    public void testTransitionTicketInvalid() throws Exception {
        //given
        Mockito.doThrow(new InvalidTicketTransitionException("Current ticket status could not be found in the system."))
                .when(ticketService).transitionTicket(TRANSITION_DTO);

        //when & then
        mvc.perform(post("/v1/tickets/SUP-1/transition")
                .content("{\n" +
                        "  \"oldStatus\": \"" + TicketStatus.OPEN.name() + "\",\n" +
                        "  \"newStatus\": \"" + TicketStatus.SOLVED.name() + "\",\n" +
                        "  \"transitionTimestamp\": \"" + NOW_ISO + "\"\n" +
                        "}")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errorCode", is(400)))
                .andExpect(jsonPath("$.errorMessage", is("Invalid ticket transition: Current ticket status could not be found in the system.")));

        verify(ticketService, times(1)).transitionTicket(TRANSITION_DTO);
    }

    @Test
    public void testTransitionTicketDateInFuture() throws Exception {
        //given: no preconditions

        //when & then
        mvc.perform(post("/v1/tickets/SUP-1/transition")
                .content("{\n" +
                        "  \"oldStatus\": \"" + TicketStatus.OPEN.name() + "\",\n" +
                        "  \"newStatus\": \"" + TicketStatus.SOLVED.name() + "\",\n" +
                        "  \"transitionTimestamp\": \"" + TestConstants.TOMORROW.atStartOfDay().format(ISO_DATE_TIME) + "\"\n" +
                        "}")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errorCode", is(400)))
                .andExpect(jsonPath("$.errorMessage", is("The value of 'transitionTimestamp' must not be in the future.")));

        verify(ticketService, times(0)).transitionTicket(TRANSITION_DTO);
    }

    @Test
    public void testTransitionTicketInvalidDate() throws Exception {
        //given: no preconditions

        //when & then
        mvc.perform(post("/v1/tickets/SUP-1/transition")
                .content("{\n" +
                        "  \"oldStatus\": \"" + TicketStatus.OPEN.name() + "\",\n" +
                        "  \"newStatus\": \"" + TicketStatus.SOLVED.name() + "\",\n" +
                        "  \"transitionTimestamp\": \"asdf\"\n" +
                        "}")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errorCode", is(400)))
                .andExpect(jsonPath("$.errorMessage", is("Parse error for value 'asdf'.")));

        verify(ticketService, times(0)).transitionTicket(TRANSITION_DTO);
    }

    @Test
    public void testTicketTransitionInvalidOldTicketStatus() throws Exception {
        //given: no preconditions

        //when & then
        mvc.perform(post("/v1/tickets/SUP-1/transition")
                .content("{\n" +
                        "  \"oldStatus\": \"asdf\",\n" +
                        "  \"newStatus\": \"" + TicketStatus.SOLVED.name() + "\",\n" +
                        "  \"transitionTimestamp\": \"" + NOW_ISO + "\"\n" +
                        "}")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errorCode", is(400)))
                .andExpect(jsonPath("$.errorMessage", is("Parse error for value 'asdf'.")));

        verify(ticketService, times(0)).transitionTicket(TRANSITION_DTO);
    }


    @Test
    public void testTicketTransitionInvalidNewTicketStatus() throws Exception {
        //given: no preconditions

        //when & then
        mvc.perform(post("/v1/tickets/SUP-1/transition")
                .content("{\n" +
                        "  \"oldStatus\": \"" + TicketStatus.OPEN.name() + "\",\n" +
                        "  \"newStatus\": \"asdf\",\n" +
                        "  \"transitionTimestamp\": \"" + NOW_ISO + "\"\n" +
                        "}")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errorCode", is(400)))
                .andExpect(jsonPath("$.errorMessage", is("Parse error for value 'asdf'.")));

        verify(ticketService, times(0)).transitionTicket(TRANSITION_DTO);
    }
}
package com.jobisoft.demo.springboot.performance;

import com.jobisoft.demo.springboot.model.DailyTicketStatisticsResult;
import com.jobisoft.demo.springboot.TestConstants;
import lombok.extern.slf4j.Slf4j;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.function.Supplier;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.lessThan;

/**
 * Note: These tests use a full spring context with an H2 db on disk to simulate the times most accurately.
 *
 * @author Johann Binder
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles(TestConstants.PERFORMANCE_TESTS_PROFILE)
public abstract class BasePerformanceTest {
    protected void testPerformance(Supplier<List<DailyTicketStatisticsResult>> functionToTest,
                                   List<DailyTicketStatisticsResult> expectedResult,
                                   long maxExecutionTime) {
        //when
        log.info("starting calculation");
        long startTime = System.currentTimeMillis();
        List<DailyTicketStatisticsResult> aggregatedDailyStatistics = functionToTest.get();
        long executionTime = System.currentTimeMillis() - startTime;
        log.info("finished calculation in {} ms", executionTime );

        //then
        assertThat(aggregatedDailyStatistics, is(expectedResult));
        assertThat(executionTime, lessThan(maxExecutionTime));
    }
}
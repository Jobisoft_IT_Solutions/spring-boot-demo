package com.jobisoft.demo.springboot.performance;

import com.jobisoft.demo.springboot.model.DailyTicketStatisticsResult;
import com.jobisoft.demo.springboot.repository.DailyTicketStatisticsRepository;
import com.jobisoft.demo.springboot.TestConstants;
import com.jobisoft.demo.springboot.model.enums.TicketStatus;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.function.Supplier;

/**
 * Note: These tests use a full spring context with an H2 db on disk to simulate the times most accurately.
 *
 * @author Johann Binder
 */
public class BaseRepositoryPerformanceTest extends BasePerformanceTest {
    @Autowired
    private DailyTicketStatisticsRepository dailyTicketStatisticsRepository;

    /**
     * This test shows that the new approach with accumulated daily data works very well even for big data sets.
     * Even if we have data for 10 years the method finishes instantly.
     */
    @Test
    @Sql("/db/testdata/daily-statistics-performance.sql")
    @Transactional
    public void testCalculateDailyTicketStatisticsPerformance() {
        //given: daily-statistics-performance.sql with around 10,000 entries
        //this represents around 3 entries per day (1 per status) for 10 years into the past from today
        //each day we get 1600 new open tickets, of which 500 are resolved and 100 are put into waiting

        //when & then
        Supplier<List<DailyTicketStatisticsResult>> functionToTest = () ->
                dailyTicketStatisticsRepository.calculateDailyTicketStatistics(TestConstants.TODAY);

        List<DailyTicketStatisticsResult> expectedResults = List.of(
                new DailyTicketStatisticsResult(TicketStatus.OPEN, 1000L * 3650),
                new DailyTicketStatisticsResult(TicketStatus.SOLVED, 500L * 3650),
                new DailyTicketStatisticsResult(TicketStatus.WAITING, 100L * 3650));

        testPerformance(functionToTest, expectedResults, 1000L);
    }
}
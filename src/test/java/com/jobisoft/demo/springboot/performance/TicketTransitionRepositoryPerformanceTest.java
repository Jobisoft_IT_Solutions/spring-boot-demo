package com.jobisoft.demo.springboot.performance;

import com.jobisoft.demo.springboot.model.DailyTicketStatisticsResult;
import com.jobisoft.demo.springboot.repository.TicketTransitionRepository;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.function.Supplier;

import static com.jobisoft.demo.springboot.TestConstants.TODAY;
import static com.jobisoft.demo.springboot.model.enums.TicketStatus.*;

/**
 * Note: These tests use a full spring context with an H2 db on disk to simulate the times most accurately.
 *
 * @author Johann Binder
 */
public class TicketTransitionRepositoryPerformanceTest extends BasePerformanceTest {
    @Autowired
    private TicketTransitionRepository ticketTransitionRepository;

    /**
     * This test should verify if the simple approach for calculating statistics over all transition rows works for large data sets.
     * It doesn't, we should use the new DailyTicketStatisticsRepository instead which contains already accumulated daily data.
     * Note: If you want to run this test you need to generate the /db/testdata/ticket-transition-performance.sql file first by executing
     * /spring-boot-demo/src/test/resources/db/testdata/scriptsgenerate_daily_statistics_testdata.sh which will have around 1 GB in size.
     */
    @Ignore
    @Test
    @Sql("/db/testdata/ticket-transition-performance.sql")
    @Transactional
    public void testCalculateDailyTicketStatisticsPerformance() {  // NOSONAR
        //given: ticket-transition-performance.sql with 5 million transition entries

        //when & then
        Supplier<List<DailyTicketStatisticsResult>> functionToTest = () ->
                ticketTransitionRepository.calculateDailyTicketStatistics(TODAY);

        List<DailyTicketStatisticsResult> expectedResults = List.of(
                new DailyTicketStatisticsResult(OPEN, 0L),
                new DailyTicketStatisticsResult(SOLVED, 1_000_000L),
                new DailyTicketStatisticsResult(WAITING, 0L));

        //this fails as it takes around 16 seconds
        testPerformance(functionToTest, expectedResults, 1000L);
    }
}
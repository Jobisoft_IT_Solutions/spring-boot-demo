package com.jobisoft.demo.springboot.repository;

import com.jobisoft.demo.springboot.model.DailyTicketStatistics;
import com.jobisoft.demo.springboot.model.DailyTicketStatisticsResult;
import com.jobisoft.demo.springboot.TestConstants;
import com.jobisoft.demo.springboot.model.enums.TicketStatus;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.fail;

/**
 * These tests use a reduced spring context so they can be executed faster.
 *
 * TODO: Further tests could be added to test all constraints.
 *
 * @author Johann Binder
 */
@RunWith(SpringRunner.class)
@DataJpaTest
@ActiveProfiles(TestConstants.REPOSITORY_TESTS_PROFILE)
public class DailyTicketStatisticsRepositoryTest {
    private List<DailyTicketStatistics> statistics = List.of(
            //10 new open
            new DailyTicketStatistics(TestConstants.YESTERDAY, TicketStatus.OPEN, 10),
            new DailyTicketStatistics(TestConstants.YESTERDAY, TicketStatus.SOLVED, 0),
            new DailyTicketStatistics(TestConstants.YESTERDAY, TicketStatus.WAITING, 0),

            //0 new open, 5 open -> solved, 5 open -> waiting
            new DailyTicketStatistics(TestConstants.TODAY, TicketStatus.OPEN, -10),
            new DailyTicketStatistics(TestConstants.TODAY, TicketStatus.SOLVED, 5),
            new DailyTicketStatistics(TestConstants.TODAY, TicketStatus.WAITING, 5),

            //10 new open, 5 open -> solved, 5 waiting -> solved
            new DailyTicketStatistics(TestConstants.TOMORROW, TicketStatus.OPEN, 5),
            new DailyTicketStatistics(TestConstants.TOMORROW, TicketStatus.SOLVED, 10),
            new DailyTicketStatistics(TestConstants.TOMORROW, TicketStatus.WAITING, -5));

    @Autowired
    private DailyTicketStatisticsRepository dailyTicketStatisticsRepository;

    /**
     * We setup 3 days of statistics for each test case.
     */
    @Before
    public void beforeTest() {
        statistics = dailyTicketStatisticsRepository.saveAll(statistics);
        dailyTicketStatisticsRepository.flush();
    }

    @Test
    public void testInsertAndFindAll() {
        //given
        List<DailyTicketStatistics> insertedStatistics = List.of(
                new DailyTicketStatistics(TestConstants.TOMORROW.plusDays(1), TicketStatus.OPEN, 5),
                new DailyTicketStatistics(TestConstants.TOMORROW.plusDays(1), TicketStatus.SOLVED, 5));
        dailyTicketStatisticsRepository.saveAll(insertedStatistics);
        dailyTicketStatisticsRepository.flush();

        //when
        List<DailyTicketStatistics> foundAll = dailyTicketStatisticsRepository.findAll();

        //then
        List<DailyTicketStatistics> expected = new ArrayList<>(statistics);
        expected.addAll(insertedStatistics);
        assertThat(foundAll, is(expected));
    }

    @Test(expected = DataIntegrityViolationException.class)
    public void testInsertOfSameDateAndTicketStatusFails() {
        //given
        DailyTicketStatistics insertedStatistics = new DailyTicketStatistics(TestConstants.TODAY, TicketStatus.OPEN, 5);

        //when
        dailyTicketStatisticsRepository.save(insertedStatistics);
        dailyTicketStatisticsRepository.flush();

        //then
        fail();
    }

    @Test
    public void testUpdateAndFindById() {
        //given
        List<DailyTicketStatistics> foundAll = dailyTicketStatisticsRepository.findAll();
        DailyTicketStatistics updatedStatistics = foundAll.get(0);
        DailyTicketStatistics statistics2 = foundAll.get(1);
        DailyTicketStatistics statistics3 = foundAll.get(2);
        updatedStatistics.setStatisticsDate(TestConstants.TOMORROW.plusDays(1));
        updatedStatistics.setTicketStatus(TicketStatus.SOLVED);
        updatedStatistics.setDiff(100);
        dailyTicketStatisticsRepository.save(updatedStatistics);
        dailyTicketStatisticsRepository.flush();

        //when
        Long statisticsId1 = updatedStatistics.getId();
        Long statisticsId2 = statistics2.getId();
        Long statisticsId3 = statistics3.getId();
        DailyTicketStatistics foundStatistics1 = dailyTicketStatisticsRepository.findById(statisticsId1).orElseThrow();
        DailyTicketStatistics foundStatistics2 = dailyTicketStatisticsRepository.findById(statisticsId2).orElseThrow();
        DailyTicketStatistics foundStatistics3 = dailyTicketStatisticsRepository.findById(statisticsId3).orElseThrow();

        //then
        verifyStatisticsValues(foundStatistics1, updatedStatistics);
        verifyStatisticsValues(foundStatistics2, statistics2);
        verifyStatisticsValues(foundStatistics3, statistics3);
    }

    private void verifyStatisticsValues(DailyTicketStatistics actual, DailyTicketStatistics expected) {
        assertThat(actual.getStatisticsDate(), is(expected.getStatisticsDate()));
        MatcherAssert.assertThat(actual.getTicketStatus(), Matchers.is(expected.getTicketStatus()));
        assertThat(actual.getDiff(), is(expected.getDiff()));
    }

    @Test
    public void testDelete() {
        //given
        DailyTicketStatistics statistics1 = dailyTicketStatisticsRepository.findAll().get(0);
        dailyTicketStatisticsRepository.delete(statistics1);
        dailyTicketStatisticsRepository.flush();

        //when
        List<DailyTicketStatistics> foundAll = dailyTicketStatisticsRepository.findAll();

        //then
        List<DailyTicketStatistics> expected = new ArrayList<>(statistics);
        expected.remove(statistics1);
        assertThat(foundAll, is(expected));
    }

    @Test
    public void testFindByStatisticsDateAndTicketStatus() {
        //given
        DailyTicketStatistics statistics = dailyTicketStatisticsRepository.findAll().get(3);

        //when
        Optional<DailyTicketStatistics> found = dailyTicketStatisticsRepository.findByStatisticsDateAndTicketStatus(
                statistics.getStatisticsDate(), statistics.getTicketStatus());

        //then
        assertThat(found.isPresent(), is(true));
        assertThat(found.get(), is(statistics));
    }

    @Test
    public void testFindByStatisticsDateAndTicketStatusNoResults() {
        //given: default setup of 3 days

        //when
        Optional<DailyTicketStatistics> found = dailyTicketStatisticsRepository.findByStatisticsDateAndTicketStatus(
                TestConstants.TOMORROW.plusDays(1), TicketStatus.OPEN);

        //then
        assertThat(found.isEmpty(), is(true));
    }

    @Test
    public void testCalculateDailyTicketStatisticsNoResults() {
        //given: default setup of 3 days

        //when
        List<DailyTicketStatisticsResult> dailyTicketStatisticsYesterday =
                dailyTicketStatisticsRepository.calculateDailyTicketStatistics(TestConstants.YESTERDAY.minusDays(1));

        //then
        assertThat(dailyTicketStatisticsYesterday, empty());
    }

    /**             yesterday       today           tomorrow
     * OPEN:        10 (10 new)     -10 (0 new)     5 (10 new)
     * SOLVED:                      5 (from open)   10 (5 from waiting, 5 from open)
     * WAITING:                     5 (from open)   -5
     */
    @Test
    public void testCalculateDailyTicketStatisticsSingleDay() {
        //given: default setup of 3 days

        //when
        List<DailyTicketStatisticsResult> dailyTicketStatisticsYesterday =
                dailyTicketStatisticsRepository.calculateDailyTicketStatistics(TestConstants.YESTERDAY);

        //then
        List<DailyTicketStatisticsResult> expectedYesterday = List.of(
                new DailyTicketStatisticsResult(TicketStatus.OPEN, 10L),
                new DailyTicketStatisticsResult(TicketStatus.SOLVED, 0L),
                new DailyTicketStatisticsResult(TicketStatus.WAITING, 0L));
        assertThat(dailyTicketStatisticsYesterday, is(expectedYesterday));
    }

    /**             yesterday       today           tomorrow
     * OPEN:        10 (10 new)     -10 (0 new)     5 (10 new)
     * SOLVED:                      5 (from open)   10 (5 from waiting, 5 from open)
     * WAITING:                     5 (from open)   -5
     */
    @Test
    public void testCalculateDailyTicketStatisticsMultiDay() {
        //given: default setup of 3 days

        //when
        List<DailyTicketStatisticsResult> dailyTicketStatisticsYesterday =
                dailyTicketStatisticsRepository.calculateDailyTicketStatistics(TestConstants.YESTERDAY);
        List<DailyTicketStatisticsResult> dailyTicketStatisticsToday =
                dailyTicketStatisticsRepository.calculateDailyTicketStatistics(TestConstants.TODAY);
        List<DailyTicketStatisticsResult> dailyTicketStatisticsTomorrow =
                dailyTicketStatisticsRepository.calculateDailyTicketStatistics(TestConstants.TOMORROW);

        //then
        List<DailyTicketStatisticsResult> expectedYesterday = List.of(
                new DailyTicketStatisticsResult(TicketStatus.OPEN, 10L),
                new DailyTicketStatisticsResult(TicketStatus.SOLVED, 0L),
                new DailyTicketStatisticsResult(TicketStatus.WAITING, 0L));
        List<DailyTicketStatisticsResult> expectedToday= List.of(
                new DailyTicketStatisticsResult(TicketStatus.OPEN, 0L),
                new DailyTicketStatisticsResult(TicketStatus.SOLVED, 5L),
                new DailyTicketStatisticsResult(TicketStatus.WAITING, 5L));
        List<DailyTicketStatisticsResult> expectedTomorrow = List.of(
                new DailyTicketStatisticsResult(TicketStatus.OPEN, 5L),
                new DailyTicketStatisticsResult(TicketStatus.SOLVED, 15L),
                new DailyTicketStatisticsResult(TicketStatus.WAITING, 0L));

        assertThat(dailyTicketStatisticsYesterday, is(expectedYesterday));
        assertThat(dailyTicketStatisticsToday, is(expectedToday));
        assertThat(dailyTicketStatisticsTomorrow, is(expectedTomorrow));
    }
}
package com.jobisoft.demo.springboot.repository;

import com.jobisoft.demo.springboot.model.Ticket;
import com.jobisoft.demo.springboot.TestConstants;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * These tests use a reduced spring context so they can be executed faster.
 *
 * TODO: Further tests could be added to test all constraints.
 *
 * @author Johann Binder
 */
@RunWith(SpringRunner.class)
@DataJpaTest
@ActiveProfiles(TestConstants.REPOSITORY_TESTS_PROFILE)
public class TicketRepositoryTest {
    private Ticket ticket1 = new Ticket("SUP-1");
    private Ticket ticket2 = new Ticket("SUP-2");
    private Ticket ticket3 = new Ticket("SUP-3");
    private List<Ticket> tickets = List.of(ticket1, ticket2, ticket3);

    @Autowired
    private TicketRepository ticketRepository;

    /**
     * We setup 3 tickets for each test case.
     */
    @Before
    public void beforeTest() {
        tickets = ticketRepository.saveAll(tickets);
        ticket1 = tickets.get(0);
        ticket2 = tickets.get(1);
        ticket3 = tickets.get(2);
        ticketRepository.flush();
    }

    @Test
    public void testInsertAndFindAll() {
        //given
        List<Ticket> insertedTickets = List.of(
                new Ticket("SUP-4"),
                new Ticket("SUP-5"));
        ticketRepository.saveAll(insertedTickets);
        ticketRepository.flush();

        //when
        List<Ticket> foundAll = ticketRepository.findAll();

        //then
        List<Ticket> expected = new ArrayList<>(tickets);
        expected.addAll(insertedTickets);
        assertThat(foundAll, is(expected));
    }

    @Test
    public void testFindById() {
        //given: default setup of 3 tickets

        //when
        Ticket foundTicket1 = ticketRepository.findById(ticket1.getId()).orElseThrow();
        Ticket foundTicket2 = ticketRepository.findById(ticket2.getId()).orElseThrow();
        Ticket foundTicket3 = ticketRepository.findById(ticket3.getId()).orElseThrow();

        //then
        assertThat(foundTicket1, is(ticket1));
        assertThat(foundTicket2, is(ticket2));
        assertThat(foundTicket3, is(ticket3));
    }

    @Test
    public void testDelete() {
        //given
        ticketRepository.delete(ticket1);
        ticketRepository.flush();

        //when
        List<Ticket> foundAll = ticketRepository.findAll();

        //then
        assertThat(foundAll, is(List.of(ticket2, ticket3)));
    }
}

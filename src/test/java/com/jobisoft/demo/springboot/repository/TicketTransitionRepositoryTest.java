package com.jobisoft.demo.springboot.repository;

import com.jobisoft.demo.springboot.model.DailyTicketStatisticsResult;
import com.jobisoft.demo.springboot.model.Ticket;
import com.jobisoft.demo.springboot.model.TicketTransition;
import com.jobisoft.demo.springboot.TestConstants;
import com.jobisoft.demo.springboot.model.enums.CalculationType;
import com.jobisoft.demo.springboot.model.enums.TicketStatus;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;

/**
 * These tests use a reduced spring context so they can be executed faster.
 *
 * TODO: Further tests could be added to test all constraints.
 *
 * @author Johann Binder
 */
@RunWith(SpringRunner.class)
@DataJpaTest
@ActiveProfiles(TestConstants.REPOSITORY_TESTS_PROFILE)
public class TicketTransitionRepositoryTest {
    private Ticket ticket1 = new Ticket("SUP-1");
    private Ticket ticket2 = new Ticket("SUP-2");
    private Ticket ticket3 = new Ticket("SUP-3");
    private List<Ticket> tickets = List.of(ticket1, ticket2, ticket3);

    private TicketTransition transition1 = new TicketTransition(ticket1, TicketStatus.OPEN, CalculationType.NEW, TestConstants.YESTERDAY.atStartOfDay());
    private TicketTransition transition2 = new TicketTransition(ticket2, TicketStatus.OPEN, CalculationType.NEW, TestConstants.TODAY.atStartOfDay());
    private TicketTransition transition3 = new TicketTransition(ticket3, TicketStatus.OPEN, CalculationType.NEW, TestConstants.TOMORROW.atStartOfDay());
    private List<TicketTransition> transitions = List.of(transition1, transition2, transition3);

    @Autowired
    private TicketRepository ticketRepository;

    @Autowired
    private TicketTransitionRepository ticketTransitionRepository;

    /**
     * We setup 3 tickets, each with 1 transition to OPEN for each test case.
     */
    @Before
    public void beforeTest() {
        tickets = ticketRepository.saveAll(tickets);
        ticket1 = tickets.get(0);
        ticket2 = tickets.get(1);
        ticket3 = tickets.get(2);
        ticketRepository.flush();

        transitions = ticketTransitionRepository.saveAll(transitions);
        transition1 = transitions.get(0);
        transition2 = transitions.get(1);
        transition3 = transitions.get(2);
        ticketTransitionRepository.flush();
    }

    @Test
    public void testInsertAndFindAll() {
        //given
        List<TicketTransition> insertedTransitions = List.of(
                new TicketTransition(ticket1, TicketStatus.OPEN, CalculationType.COMPENSATE, LocalDateTime.now()),
                new TicketTransition(ticket1, TicketStatus.SOLVED, CalculationType.NEW, LocalDateTime.now()));
        ticketTransitionRepository.saveAll(insertedTransitions);
        ticketTransitionRepository.flush();

        //when
        List<TicketTransition> foundAll = ticketTransitionRepository.findAll();

        //then
        List<TicketTransition> expected = new ArrayList<>(transitions);
        expected.addAll(insertedTransitions);
        assertThat(foundAll, is(expected));
    }

    @Test
    public void testUpdateAndFindById() {
        //given
        List<TicketTransition> foundAll = ticketTransitionRepository.findAll();
        TicketTransition updatedTransition = foundAll.get(0);
        updatedTransition.setTicket(ticket2);
        updatedTransition.setTicketStatus(TicketStatus.SOLVED);
        updatedTransition.setCalculationType(CalculationType.COMPENSATE);
        updatedTransition.setTransitionTimestamp(LocalDateTime.now());
        ticketTransitionRepository.save(updatedTransition);
        ticketTransitionRepository.flush();

        //when
        Long transitionId1 = foundAll.get(0).getId();
        Long transitionId2 = foundAll.get(1).getId();
        Long transitionId3 = foundAll.get(2).getId();
        TicketTransition foundTransition1 = ticketTransitionRepository.findById(transitionId1).orElseThrow();
        TicketTransition foundTransition2 = ticketTransitionRepository.findById(transitionId2).orElseThrow();
        TicketTransition foundTransition3 = ticketTransitionRepository.findById(transitionId3).orElseThrow();

        //then
        verifyTransitionValues(foundTransition1, updatedTransition);
        verifyTransitionValues(foundTransition2, transition2);
        verifyTransitionValues(foundTransition3, transition3);
    }

    private void verifyTransitionValues(TicketTransition actual, TicketTransition expected) {
        assertThat(actual.getTicket(), is(expected.getTicket()));
        MatcherAssert.assertThat(actual.getTicketStatus(), Matchers.is(expected.getTicketStatus()));
        MatcherAssert.assertThat(actual.getCalculationType(), Matchers.is(expected.getCalculationType()));
        assertThat(actual.getTransitionTimestamp(), is(expected.getTransitionTimestamp()));
        assertThat(actual.getTransitionTime(), is(expected.getTransitionTime()));
        assertThat(actual.getTransitionDate(), is(expected.getTransitionDate()));
    }

    @Test
    public void testDelete() {
        //given
        ticketTransitionRepository.delete(transition1);
        ticketTransitionRepository.flush();

        //when
        List<TicketTransition> foundAll = ticketTransitionRepository.findAll();

        //then
        assertThat(foundAll, is(List.of(transition2, transition3)));
    }

    @Test
    public void testCalculateDailyTicketStatisticsNoResults() {
        //given: default setup of 3 transitions

        //when
        List<DailyTicketStatisticsResult> dailyTicketStatisticsYesterday =
                ticketTransitionRepository.calculateDailyTicketStatistics(TestConstants.YESTERDAY.minusDays(1));

        //then
        assertThat(dailyTicketStatisticsYesterday, empty());
    }

    /**             yesterday       today           tomorrow
     * Ticket 1:    OPEN -> SOLVED
     * Ticket 2:                    OPEN
     * Ticket 3:                                    OPEN
     */
    @Test
    public void testCalculateDailyTicketStatisticsSingleDay() {
        //given
        List<TicketTransition> transitions = List.of(
                new TicketTransition(ticket1, TicketStatus.OPEN, CalculationType.COMPENSATE, TestConstants.YESTERDAY.atStartOfDay()),
                new TicketTransition(ticket1, TicketStatus.SOLVED, CalculationType.NEW, TestConstants.YESTERDAY.atStartOfDay()));
        ticketTransitionRepository.saveAll(transitions);
        ticketTransitionRepository.flush();

        //when
        List<DailyTicketStatisticsResult> dailyTicketStatisticsYesterday =
                ticketTransitionRepository.calculateDailyTicketStatistics(TestConstants.YESTERDAY);

        //then
        List<DailyTicketStatisticsResult> expectedYesterday = List.of(
                new DailyTicketStatisticsResult(TicketStatus.OPEN, 0L),
                new DailyTicketStatisticsResult(TicketStatus.SOLVED, 1L));
        assertThat(dailyTicketStatisticsYesterday, is(expectedYesterday));
    }

    /**             yesterday       today           tomorrow
     * Ticket 1:    OPEN            WAITING         SOLVED
     * Ticket 2:                    OPEN -> SOLVED
     * Ticket 3:                                    OPEN
     */
    @Test
    public void testCalculateDailyTicketStatisticsMultiDay() {
        //given
        List<TicketTransition> transitions = List.of(
                new TicketTransition(ticket1, TicketStatus.OPEN, CalculationType.COMPENSATE, TestConstants.TODAY.atStartOfDay()),
                new TicketTransition(ticket1, TicketStatus.WAITING, CalculationType.NEW, TestConstants.TODAY.atStartOfDay()),
                new TicketTransition(ticket1, TicketStatus.WAITING, CalculationType.COMPENSATE, TestConstants.TOMORROW.atStartOfDay()),
                new TicketTransition(ticket1, TicketStatus.SOLVED, CalculationType.NEW, TestConstants.TOMORROW.atStartOfDay()),

                new TicketTransition(ticket2, TicketStatus.OPEN, CalculationType.COMPENSATE, TestConstants.TODAY.atStartOfDay()),
                new TicketTransition(ticket2, TicketStatus.SOLVED, CalculationType.NEW, TestConstants.TODAY.atStartOfDay()));
        ticketTransitionRepository.saveAll(transitions);
        ticketTransitionRepository.flush();

        //when
        List<DailyTicketStatisticsResult> dailyTicketStatisticsYesterday =
                ticketTransitionRepository.calculateDailyTicketStatistics(TestConstants.YESTERDAY);
        List<DailyTicketStatisticsResult> dailyTicketStatisticsToday =
                ticketTransitionRepository.calculateDailyTicketStatistics(TestConstants.TODAY);
        List<DailyTicketStatisticsResult> dailyTicketStatisticsTomorrow =
                ticketTransitionRepository.calculateDailyTicketStatistics(TestConstants.TOMORROW);

        //then
        List<DailyTicketStatisticsResult> expectedYesterday = List.of(
                new DailyTicketStatisticsResult(TicketStatus.OPEN, 1L));
        List<DailyTicketStatisticsResult> expectedToday= List.of(
                new DailyTicketStatisticsResult(TicketStatus.OPEN, 0L),
                new DailyTicketStatisticsResult(TicketStatus.SOLVED, 1L),
                new DailyTicketStatisticsResult(TicketStatus.WAITING, 1L));
        List<DailyTicketStatisticsResult> expectedTomorrow = List.of(
                new DailyTicketStatisticsResult(TicketStatus.OPEN, 1L),
                new DailyTicketStatisticsResult(TicketStatus.SOLVED, 2L),
                new DailyTicketStatisticsResult(TicketStatus.WAITING, 0L));

        assertThat(dailyTicketStatisticsYesterday, is(expectedYesterday));
        assertThat(dailyTicketStatisticsToday, is(expectedToday));
        assertThat(dailyTicketStatisticsTomorrow, is(expectedTomorrow));
    }
}
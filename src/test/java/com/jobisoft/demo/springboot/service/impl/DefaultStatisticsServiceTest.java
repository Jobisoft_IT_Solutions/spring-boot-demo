package com.jobisoft.demo.springboot.service.impl;


import com.jobisoft.demo.springboot.model.DailyTicketStatisticsResult;
import com.jobisoft.demo.springboot.model.enums.TicketStatus;
import com.jobisoft.demo.springboot.repository.DailyTicketStatisticsRepository;
import com.jobisoft.demo.springboot.TestConstants;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;

/**
 * These tests mock away the persistence layer completely.
 *
 * @author Johann Binder
 */
@RunWith(SpringRunner.class)
public class DefaultStatisticsServiceTest {
    @MockBean
    private DailyTicketStatisticsRepository dailyTicketStatisticsRepository;

    private DefaultStatisticsService defaultStatisticsService;

    @Before
    public void beforeTest() {
        defaultStatisticsService = new DefaultStatisticsService(dailyTicketStatisticsRepository);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetDailyTicketStatisticsWithNull() {
        //given
        when(dailyTicketStatisticsRepository.calculateDailyTicketStatistics(null))
                .thenThrow(new IllegalStateException());

        //when
        defaultStatisticsService.getDailyTicketStatistics(null);

        //then
        fail();
    }

    @Test
    public void testGetDailyTicketStatisticsWithNoResults() {
        //given
        when(dailyTicketStatisticsRepository.calculateDailyTicketStatistics(TestConstants.TODAY))
                .thenReturn(Collections.emptyList());

        //when
        Map<TicketStatus, Long> result = defaultStatisticsService.getDailyTicketStatistics(TestConstants.TODAY);

        //then
        assertThat(result.isEmpty(), is(true));
    }

    @Test
    public void testGetDailyTicketStatisticsWithResults() {
        //given
        when(dailyTicketStatisticsRepository.calculateDailyTicketStatistics(TestConstants.TODAY))
                .thenReturn(List.of(
                        new DailyTicketStatisticsResult(TicketStatus.OPEN, 1L),
                        new DailyTicketStatisticsResult(TicketStatus.SOLVED, 2L),
                        new DailyTicketStatisticsResult(TicketStatus.WAITING, 3L)
                ));

        //when
        Map<TicketStatus, Long> result = defaultStatisticsService.getDailyTicketStatistics(TestConstants.TODAY);

        //then
        assertThat(result.size(), is(3));
        assertThat(result.get(TicketStatus.OPEN), is(1L));
        assertThat(result.get(TicketStatus.SOLVED), is(2L));
        assertThat(result.get(TicketStatus.WAITING), is(3L));
    }

}

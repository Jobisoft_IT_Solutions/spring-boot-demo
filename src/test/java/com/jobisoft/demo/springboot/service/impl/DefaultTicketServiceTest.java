package com.jobisoft.demo.springboot.service.impl;


import com.jobisoft.demo.springboot.dto.TicketDTO;
import com.jobisoft.demo.springboot.dto.TicketTransitionDTO;
import com.jobisoft.demo.springboot.model.DailyTicketStatistics;
import com.jobisoft.demo.springboot.model.Ticket;
import com.jobisoft.demo.springboot.model.TicketTransition;
import com.jobisoft.demo.springboot.repository.DailyTicketStatisticsRepository;
import com.jobisoft.demo.springboot.repository.TicketRepository;
import com.jobisoft.demo.springboot.repository.TicketTransitionRepository;
import com.jobisoft.demo.springboot.service.exception.InvalidTicketTransitionException;
import com.jobisoft.demo.springboot.service.exception.TicketDoesAlreadyExistException;
import com.jobisoft.demo.springboot.service.exception.TicketNotFoundException;
import com.jobisoft.demo.springboot.service.exception.TicketTransitionDoesAlreadyExistException;
import com.jobisoft.demo.springboot.TestConstants;
import com.jobisoft.demo.springboot.model.enums.CalculationType;
import com.jobisoft.demo.springboot.model.enums.TicketStatus;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.times;

/**
 * These tests mock away the persistence layer completely.
 *
 * @author Johann Binder
 */
@RunWith(SpringRunner.class)
public class DefaultTicketServiceTest {
    private static final LocalDateTime TIMESTAMP = TestConstants.TODAY.atStartOfDay();

    private final Ticket TICKET = new Ticket("SUP-1");
    private final TicketTransition TRANSITION = new TicketTransition(TICKET, TicketStatus.OPEN, CalculationType.NEW, TIMESTAMP);
    private final DailyTicketStatistics STATISTICS = new DailyTicketStatistics(TestConstants.TODAY, TicketStatus.OPEN, 1);
    private final TicketDTO TICKET_DTO = new TicketDTO(TICKET.getId(), TicketStatus.OPEN, TIMESTAMP);
    private final TicketTransitionDTO TRANSITION_DTO = new TicketTransitionDTO(TICKET.getId(), TicketStatus.OPEN, TicketStatus.SOLVED, TIMESTAMP);

    @MockBean
    private TicketRepository ticketRepository;

    @MockBean
    private TicketTransitionRepository ticketTransitionRepository;

    @MockBean
    private DailyTicketStatisticsRepository dailyTicketStatisticsRepository;

    private DefaultTicketService defaultTicketService;

    @Before
    public void beforeTest() {
        defaultTicketService = new DefaultTicketService(
                ticketRepository, ticketTransitionRepository, dailyTicketStatisticsRepository);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAddTicketWithNull() {
        //given: TICKET service with all repositories mocked

        //when
        defaultTicketService.addTicket(null);

        //then
        fail();
    }

    @Test(expected = TicketDoesAlreadyExistException.class)
    public void testAddTicketIfTicketAlreadyExists() {
        //given
        when(ticketRepository.findById(TICKET.getId()))
                .thenReturn(Optional.of(TICKET));

        //when
        defaultTicketService.addTicket(TICKET_DTO);

        //then
        fail();
    }

    @Test
    public void testAddTicketSuccessfulWithoutExistingStatistics() {
        //given
        when(ticketRepository.findById(TICKET.getId()))
                .thenReturn(Optional.empty());
        when(ticketRepository.save(TICKET))
                .thenReturn(TICKET);
        when(dailyTicketStatisticsRepository.findByStatisticsDateAndTicketStatus(TestConstants.TODAY, TicketStatus.OPEN))
                .thenReturn(Optional.empty());

        //when
        defaultTicketService.addTicket(TICKET_DTO);

        //then
        verify(ticketRepository, times(1)).findById(TICKET.getId());
        verify(ticketRepository, times(1)).save(TICKET);
        verify(ticketRepository).save(
                argThat((Ticket ticket) -> ticket.getId().equals(this.TICKET.getId())));

        verify(ticketTransitionRepository, times(1)).save(TRANSITION);
        verify(ticketTransitionRepository).save(argThat(TRANSITION::equalsAllFields));

        verify(dailyTicketStatisticsRepository, times(1))
                .findByStatisticsDateAndTicketStatus(TestConstants.TODAY, TicketStatus.OPEN);
        verify(dailyTicketStatisticsRepository, times(1)).save(STATISTICS);
        verify(dailyTicketStatisticsRepository).save(
                argThat((DailyTicketStatistics statistics) -> statistics.getDiff() == 1));
    }

    @Test
    public void testAddTicketSuccessfulWithExistingStatistics() {
        //given
        when(ticketRepository.findById(TICKET.getId()))
                .thenReturn(Optional.empty());
        when(ticketRepository.save(TICKET))
                .thenReturn(TICKET);
        when(dailyTicketStatisticsRepository.findByStatisticsDateAndTicketStatus(TestConstants.TODAY, TicketStatus.OPEN))
                .thenReturn(Optional.of(STATISTICS));

        //when
        defaultTicketService.addTicket(TICKET_DTO);

        //then
        verify(ticketRepository, times(1)).findById(TICKET.getId());
        verify(ticketRepository, times(1)).save(TICKET);
        verify(ticketRepository).save(
                argThat((Ticket ticket) -> ticket.getId().equals(this.TICKET.getId())));

        verify(ticketTransitionRepository, times(1)).save(TRANSITION);
        verify(ticketTransitionRepository).save(argThat(TRANSITION::equalsAllFields));

        verify(dailyTicketStatisticsRepository, times(1))
                .findByStatisticsDateAndTicketStatus(TestConstants.TODAY, TicketStatus.OPEN);
        verify(dailyTicketStatisticsRepository, times(1)).save(STATISTICS);
        verify(dailyTicketStatisticsRepository).save(
                argThat((DailyTicketStatistics statistics) -> statistics.getDiff() == 2));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testTransitionTicketWithNull() {
        //given: TICKET service with all repositories mocked

        //when
        defaultTicketService.transitionTicket(null);

        //then
        fail();
    }

    @Test(expected = TicketNotFoundException.class)
    public void testTransitionTicketIfTicketDoesNotExist() {
        //given
        when(ticketRepository.findById(TICKET.getId()))
                .thenReturn(Optional.empty());

        //when
        defaultTicketService.transitionTicket(TRANSITION_DTO);

        //then
        fail();
    }

    @Test(expected = TicketTransitionDoesAlreadyExistException.class)
    public void testTransitionTicketWithAlreadyExistingTransitions() {
        //given
        List<TicketTransition> transitions = List.of(
                new TicketTransition(TICKET, TicketStatus.OPEN, CalculationType.COMPENSATE, TIMESTAMP),
                new TicketTransition(TICKET, TicketStatus.SOLVED, CalculationType.NEW, TIMESTAMP)
        );

        when(ticketRepository.findById(TICKET.getId()))
                .thenReturn(Optional.of(TICKET));
        when(ticketTransitionRepository.findTopByTicketOrderByIdDesc(TICKET))
                .thenReturn(Optional.of(TRANSITION));
        when(ticketTransitionRepository.saveAll(transitions))
                .thenThrow(DataIntegrityViolationException.class);

        //when
        defaultTicketService.transitionTicket(TRANSITION_DTO);

        //then
        fail();
    }

    @Test(expected = InvalidTicketTransitionException.class)
    public void testTransitionTicketWithNoCurrentTransitionInPlace() {
        //given
        when(ticketRepository.findById(TICKET.getId()))
                .thenReturn(Optional.of(TICKET));
        when(ticketTransitionRepository.findTopByTicketOrderByIdDesc(TICKET))
                .thenReturn(Optional.empty());

        //when
        defaultTicketService.transitionTicket(TRANSITION_DTO);

        //then
        fail();
    }

    @Test(expected = InvalidTicketTransitionException.class)
    public void testTransitionTicketWithMoreCurrentTransitionInPlace() {
        //given
        when(ticketRepository.findById(TICKET.getId()))
                .thenReturn(Optional.of(TICKET));
        when(ticketTransitionRepository.findTopByTicketOrderByIdDesc(TICKET))
                .thenReturn(Optional.of(new TicketTransition(
                        TICKET, TicketStatus.OPEN, CalculationType.NEW, TestConstants.TOMORROW.atStartOfDay()
                )));

        //when
        defaultTicketService.transitionTicket(TRANSITION_DTO);

        //then
        fail();
    }

    @Test(expected = InvalidTicketTransitionException.class)
    public void testTransitionTicketWithInvalidStatusInPlace() {
        //given
        when(ticketRepository.findById(TICKET.getId()))
                .thenReturn(Optional.of(TICKET));
        when(ticketTransitionRepository.findTopByTicketOrderByIdDesc(TICKET))
                .thenReturn(Optional.of(new TicketTransition(
                        TICKET, TicketStatus.WAITING, CalculationType.NEW, TIMESTAMP
                )));

        //when
        defaultTicketService.transitionTicket(TRANSITION_DTO);

        //then
        fail();
    }

    @Test
    public void testTransitionTicketSuccessfulWithoutExistingStatistics() {
        //given
        List<TicketTransition> transitions = List.of(
                new TicketTransition(TICKET, TicketStatus.OPEN, CalculationType.COMPENSATE, TIMESTAMP),
                new TicketTransition(TICKET, TicketStatus.SOLVED, CalculationType.NEW, TIMESTAMP)
        );

        when(ticketRepository.findById(TICKET.getId()))
                .thenReturn(Optional.of(TICKET));
        when(ticketTransitionRepository.findTopByTicketOrderByIdDesc(TICKET))
                .thenReturn(Optional.of(TRANSITION));
        when(dailyTicketStatisticsRepository.findByStatisticsDateAndTicketStatus(TestConstants.TODAY, TicketStatus.OPEN))
                .thenReturn(Optional.empty());
        when(dailyTicketStatisticsRepository.findByStatisticsDateAndTicketStatus(TestConstants.TODAY, TicketStatus.SOLVED))
                .thenReturn(Optional.empty());

        //when
        defaultTicketService.transitionTicket(TRANSITION_DTO);

        //then
        verify(ticketRepository, times(1)).findById(TICKET.getId());

        verify(ticketTransitionRepository, times(1)).saveAll(transitions);
        verify(ticketTransitionRepository).saveAll(
                argThat((List<TicketTransition> list) ->
                        transitions.get(0).equalsAllFields(list.get(0)) &&
                        transitions.get(1).equalsAllFields(list.get(1))));

        verify(dailyTicketStatisticsRepository, times(1))
                .findByStatisticsDateAndTicketStatus(TestConstants.TODAY, TicketStatus.OPEN);
        verify(dailyTicketStatisticsRepository, times(1))
                .findByStatisticsDateAndTicketStatus(TestConstants.TODAY, TicketStatus.SOLVED);

        verify(dailyTicketStatisticsRepository, times(2)).save(any());
        verify(dailyTicketStatisticsRepository).save(
                argThat((DailyTicketStatistics statistics) ->
                        statistics.getTicketStatus() == TicketStatus.OPEN &&
                        statistics.getDiff() == -1));
        verify(dailyTicketStatisticsRepository).save(
                argThat((DailyTicketStatistics statistics) ->
                        statistics.getTicketStatus() == TicketStatus.SOLVED &&
                                statistics.getDiff() == 1));
    }

    @Test
    public void testTransitionTicketSuccessfulWithExistingStatistics() {
        //given
        List<TicketTransition> transitions = List.of(
                new TicketTransition(TICKET, TicketStatus.OPEN, CalculationType.COMPENSATE, TIMESTAMP),
                new TicketTransition(TICKET, TicketStatus.SOLVED, CalculationType.NEW, TIMESTAMP)
        );
        DailyTicketStatistics openStatistics = new DailyTicketStatistics(TestConstants.TODAY, TicketStatus.OPEN, 5);
        DailyTicketStatistics solvedStatistics = new DailyTicketStatistics(TestConstants.TODAY, TicketStatus.SOLVED, 10);

        when(ticketRepository.findById(TICKET.getId()))
                .thenReturn(Optional.of(TICKET));
        when(ticketTransitionRepository.findTopByTicketOrderByIdDesc(TICKET))
                .thenReturn(Optional.of(TRANSITION));
        when(dailyTicketStatisticsRepository.findByStatisticsDateAndTicketStatus(TestConstants.TODAY, TicketStatus.OPEN))
                .thenReturn(Optional.of(openStatistics));
        when(dailyTicketStatisticsRepository.findByStatisticsDateAndTicketStatus(TestConstants.TODAY, TicketStatus.SOLVED))
                .thenReturn(Optional.of(solvedStatistics));

        //when
        defaultTicketService.transitionTicket(TRANSITION_DTO);

        //then
        verify(ticketRepository, times(1)).findById(TICKET.getId());

        verify(ticketTransitionRepository, times(1)).saveAll(transitions);
        verify(ticketTransitionRepository).saveAll(
                argThat((List<TicketTransition> list) ->
                        transitions.get(0).equalsAllFields(list.get(0)) &&
                        transitions.get(1).equalsAllFields(list.get(1))));

        verify(dailyTicketStatisticsRepository, times(1))
                .findByStatisticsDateAndTicketStatus(TestConstants.TODAY, TicketStatus.OPEN);
        verify(dailyTicketStatisticsRepository, times(1))
                .findByStatisticsDateAndTicketStatus(TestConstants.TODAY, TicketStatus.SOLVED);

        verify(dailyTicketStatisticsRepository, times(2)).save(any());
        verify(dailyTicketStatisticsRepository).save(
                argThat((DailyTicketStatistics statistics) ->
                        statistics.getStatisticsDate().equals(TestConstants.TODAY) &&
                        statistics.getTicketStatus() == TicketStatus.OPEN &&
                        statistics.getDiff() == 4));
        verify(dailyTicketStatisticsRepository).save(
                argThat((DailyTicketStatistics statistics) ->
                        statistics.getStatisticsDate().equals(TestConstants.TODAY) &&
                        statistics.getTicketStatus() == TicketStatus.SOLVED &&
                        statistics.getDiff() == 11));
    }
}

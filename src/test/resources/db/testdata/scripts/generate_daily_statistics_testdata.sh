#!/usr/bin/env bash

sqlFile=../daily-statistics-performance.sql
echo "" > ${sqlFile}

# calculate 10 years in the past from today
# each day we get 1600 new open tickets, of which 500 are resolved and 100 are put into waiting
today=$(date -I)
for i in {0..3649}; do
    date=$(date -I -d "$today - ${i} day")
    echo "insert into daily_ticket_statistics (statistics_date, ticket_status, diff) values ('${date}', 0, 1000);" >> ${sqlFile}
    echo "insert into daily_ticket_statistics (statistics_date, ticket_status, diff) values ('${date}', 1, 500);" >> ${sqlFile}
    echo "insert into daily_ticket_statistics (statistics_date, ticket_status, diff) values ('${date}', 2, 100);" >> ${sqlFile}
done
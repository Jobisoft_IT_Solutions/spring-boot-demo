#!/usr/bin/env bash

sqlFile=../ticket-transition-performance.sql
echo "" > ${sqlFile}

for i in {1..1000000}; do
    echo "insert into ticket values ('SUP-$i');" >> ${sqlFile}
    echo "insert into ticket_transition (ticket_id, ticket_status, calculation_value, transition_date, transition_time, transition_timestamp) values ('SUP-$i', 0, 1, '2019-03-07', '14:00:00.000', '2019-03-07 14:00:00.000');" >> ${sqlFile}
    echo "insert into ticket_transition (ticket_id, ticket_status, calculation_value, transition_date, transition_time, transition_timestamp) values ('SUP-$i', 0, -1, '2019-03-07', '14:00:00.000', '2019-03-07 14:00:00.000');" >> ${sqlFile}
    echo "insert into ticket_transition (ticket_id, ticket_status, calculation_value, transition_date, transition_time, transition_timestamp) values ('SUP-$i', 2, 1, '2019-03-07', '14:00:00.000', '2019-03-07 14:00:00.000');" >> ${sqlFile}
    echo "insert into ticket_transition (ticket_id, ticket_status, calculation_value, transition_date, transition_time, transition_timestamp) values ('SUP-$i', 2, -1, '2019-03-07', '14:00:00.000', '2019-03-07 14:00:00.000');" >> ${sqlFile}
    echo "insert into ticket_transition (ticket_id, ticket_status, calculation_value, transition_date, transition_time, transition_timestamp) values ('SUP-$i', 1, 1, '2019-03-07', '14:00:00.000', '2019-03-07 14:00:00.000');" >> ${sqlFile}
done